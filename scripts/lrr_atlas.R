# <!-- coding: utf-8 -->
#
# quelques fonctions pour la liste rouge régionale avec les données faune-bretagne
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# la génération du fichier Tex
# source("geo/scripts/lrr.R");atlas_especes()
atlas_especes <- function() {
  carp("début")
  df <- atlas_especes_lire()
  atlasDir <- sprintf("%s/atlas", texDir)
  dir.create(atlasDir, showWarnings = FALSE, recursive = TRUE)
  texFic <- sprintf("%s/%s", texDir, "atlas_especes.tex")
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
#
# le template tex
  dsn <- sprintf("%s/atlas_especes_tpl.tex", texDir)
  template <- readLines(dsn)
  for(i in 1:nrow(df) ) {
    code <- df[i, 'code']
    espece <- df[i, 'espece']
    tpl <- template
    tpl <-  gsub("\\{\\{espece\\}\\}", espece, tpl)
    tpl <-  gsub("\\{\\{code\\}\\}", code, tpl)
    tex <- append(tex, tpl)
  }
  write(tex, file = TEX, append = FALSE)
  carp(" texFic: %s", texFic)
  carp(" fin")
}
#
# source("geo/scripts/lrr.R");atlas_faune_especes()
atlas_faune_especes <- function() {
  carp("début")
  df <- atlas_especes_lire()
  atlasDir <- sprintf("%s/atlas", texDir)
  dir.create(atlasDir, showWarnings = FALSE, recursive = TRUE)
  grille.sf <- gob_carres_35_lire()
  for(i in 1:nrow(df) ) {
    code <- df[i, 'code']
    espece <- df[i, 'espece']
#    atlas_faune_espece(code)
    df1 <- faune_especes_qualitatif_utm(code, grille.sf)
    carp('nrow: %s', nrow(df1))
    a <- 'a2004'
    df2 <- filter(df1, periode==a)
    atlas_faune_espece(grille.sf, df2)
    dsn <- sprintf("%s/faune_%s_%s.pdf", atlasDir, code, a)
    dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
    invisible(dev.off())
#    dev.copy(pdf, dsn)
    a <- 'a2015'
    df2 <- filter(df1, periode==a)
    atlas_faune_espece(grille.sf, df2)
    dsn <- sprintf("%s/faune_%s_%s.pdf", atlasDir, code, a)
    dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
    invisible(dev.off())
#    dev.copy(pdf, dsn)
  }
  carp("fin")
}
atlas_faune_espece <- function(nc, df) {
  carp("début nrow: %s", nrow(df))
  library(sf)
  library(tidyverse)
  nc <- left_join(nc, df, by=c('utm'='utm')) %>%
    glimpse()
  w <- 2048
  h <- 2048
  dev.new(width = w, height = h, units = "px", pointsize = 12)
  opar <- par(mar=c(0,0,0,0), oma=c(0,0,0,0))
  plot(st_geometry(nc), col=nc$couleur)
}
# atlas en mailles utm pour une espèce
# source("geo/scripts/lrr.R");atlas_espece()
atlas_faune_espece_periode <- function(code_espece=388) {
  carp("début")
  library(sf)
  library(tidyverse)
  epsg <- 32630
  grille.sf <- couches_grille_departement_lire()
  espece.sf <- couches_espece_lire(code_espece) %>%
    st_transform(epsg)
  df <- st_intersection(espece.sf, grille.sf) %>%
    glimpse() %>%
    st_set_geometry(NULL) %>%
    select(date_year, atlas_code, ncarre) %>%
    glimpse()
  les_periodes.df <- read.table(text="periode;annee
atlas;2004
atlas;2005
atlas;2006
atlas;2007
atlas;2008
lrr;2015
lrr;2016
lrr;2017
lrr;2018
", header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="") %>%
    glimpse()
  les_codes.df <- read.table(text="statut;code
possible;2
possible;3
probable;4
probable;5
probable;6
probable;7
probable;8
probable;9
probable;10
certain;11
certain;12
certain;13
certain;14
certain;15
certain;16
certain;17
certain;18
certain;19
", header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="") %>%
    glimpse()
  les_tendances.df <- read.table(text="tendance;code
CER_CER;darkgrey
PRO_PRO;grey
CER_PRO;pink
CER_;red
PRO_;darkred
_CER;green
_PRO;darkgreen
PRO_CER;darkgreen
", header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="") %>%
    glimpse()
  df1 <- df %>%
    left_join(les_periodes.df, by=c("date_year"="annee")) %>%
    left_join(les_codes.df, by=c("atlas_code"="code")) %>%
    drop_na(periode, statut) %>%
    group_by(ncarre, periode, statut) %>%
    summarize(nb=n()) %>%
    mutate(periode_statut=paste0(periode, '_', statut))
  carp("suite")
  df2 <- df1 %>%
    ungroup() %>%
    select(ncarre, periode_statut, nb) %>%
    spread(periode_statut, nb, 0) %>%
    mutate(atlas=case_when(atlas_certain>0 ~ 'CER', atlas_probable>0 ~ 'PRO', TRUE~''), ) %>%
    mutate(lrr=case_when(lrr_certain>0 ~ 'CER', lrr_probable>0 ~ 'PRO', TRUE~''), ) %>%
    mutate(tendance=paste0(atlas, '_', lrr)) %>%
    left_join(les_tendances.df) %>%
    print(n=100) %>%
    glimpse()
#  stop("*****")
  grille.sf <- left_join(grille.sf, df2) %>%
    mutate(code=ifelse(is.na(code), 'white', code)) %>%
    glimpse()
  w <- 2048
  h <- 2048
  dev.new( width = w, height = h, units = "px", pointsize = 12)
  opar <- par(mar=c(0,0,0,0), oma=c(0,0,0,0))
  plot(st_geometry(grille.sf), col=grille.sf$code)
  text(st_coordinates(st_centroid(grille.sf)), labels=grille.sf$tendance, font = 2, cex= .5, col='black')
  carp("fin")
}
#
# source("geo/scripts/lrr.R");atlas_gob_especes()
atlas_gob_especes <- function() {
  carp("début")
  df <- atlas_especes_lire()
  qualitatif.df <- gob_qualitafif_lire()
  nc <- gob_carres_35_lire()
  qualitatif.df <- qualitatif.df %>%
    filter(carre %in% nc$name) %>%
    glimpse()
  atlasDir <- sprintf("%s/atlas", texDir)
  dir.create(atlasDir, showWarnings = FALSE, recursive = TRUE)
  for(i in 1:nrow(df) ) {
    code <- df[i, 'code']
    Espece <- df[i, 'espece']
    carp('Espece: %s', Espece)
    df1 <- qualitatif.df %>%
      filter(espece == Espece) %>%
      glimpse()
    atlas_gob_espece(nc, df1)
    dsn <- sprintf("%s/gob_%s.pdf", atlasDir, code)
    dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
    invisible(dev.off())

#    dev.copy(pdf, dsn)
#    stop("***")
  }
  carp(" fin")
}

atlas_gob_espece <- function(nc, df) {
  carp("début")
  library(sf)
  library(tidyverse)
  les_couleurs.df <- read.table(text="code;couleur
0;white
1;yellow
2;orange
3;red
", header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="") %>%
    glimpse()
  nc <- left_join(nc, df, by=c('name'='carre')) %>%
    mutate(code=ifelse(is.na(code), 0, code)) %>%
    left_join(les_couleurs.df) %>%
    glimpse()
  w <- 2048
  h <- 2048
  dev.new(width = w, height = h, units = "px", pointsize = 12)
  opar <- par(mar=c(0,0,0,0), oma=c(0,0,0,0))
  plot(st_geometry(nc), col=nc$couleur)
#  print(nc, n=100);  stop('***')
}
#
# fonctions génériques
# ==================================================
#
# la liste des espèces à prendre en compte
atlas_especes_lire <- function() {
  carp("début")
  library(tidyverse)
  df <- read.table(text="code;espece
#233;Chevalier guignette
511;Bouvreuil pivoine
463;Pipit farlouse
388;Rossignol philomèle
395;Rougequeue à front blanc
498;Grosbec casse-noyaux
", header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  df$espece <- iconv(df$espece, 'UTF-8')
  df <- filter(df, ! grepl('^#', code))
  return(invisible(df))
}

